// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

/**
 * Movement component meant for use with Pawns.
 */

#pragma once

#include "GameFramework/CharacterMovementComponent.h"

#include "ShooterCharacterMovement.generated.h"

UCLASS()
class UShooterCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_UCLASS_BODY()

	virtual float GetMaxSpeed() const override;
	virtual void CalcVelocity(float DeltaTime, float Friction, bool bFluid, float BrakingDeceleration) override;
	virtual void ApplyComponentBraking(float DeltaTime, float Friction, float BrakingDeceleration, bool ForwardOrStrafe);
	virtual void OnMovementModeChanged(EMovementMode PreviousMovementMode, uint8 PreviousCustomMode) override;

	/** Skip braking for one tick on ground */
	bool bBrakingFrameTolerated;

	/** Our last z movement from physics */
	float LastPhysicsZ;
	/** last impact normal from floor */
	FVector LastRampNormal;
	/** last floor dot */
	float LastFloorDot;
};

